// if(module.hot) {
//   module.hot.accept('./common/navbar', function () {
//     require('./common/navbar');
//   });
// }
// window.$ = window.jQuery = require('jquery');
// window.slick = require('slick-carousel');

require('./modules/browserDetect');
require('bootstrap-hover-dropdown');
require('./bootstrap/collapse');
require('./bootstrap/tab');


const imagesContext = require.context('images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

import '../sass/main.scss';

if(NODE_ENV === 'development') {
  require('../views/pages/company.twig');
}


(function ($) {
  $(document).ready(function () {

  });

  $(window).on('load', function () {
    require('./modules/header');
    require('./modules/slider');

    const $sliderRange = $('.slider-range');
    const sliderRangeNumber = 3;
    let sliderRangeActive = 0;

    $( function() {
      $("#slider-range").slider({
        range: 'min',
        animate: "slow",
        min: 0,
        max: sliderRangeNumber - 1,
        change: function( event, ui ) {
          let activeTab = `[data-range='${sliderRangeActive}']`
          let nextTab = `[data-range='${ui.value}']`

          $sliderRange.find(activeTab).removeClass(`active`);
          $sliderRange.find(nextTab).addClass(`active`);

          sliderRangeActive = ui.value;
        }
      });
    });
  });
})(jQuery);
