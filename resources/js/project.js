// if(module.hot) {
//   module.hot.accept('./common/navbar', function () {
//     require('./common/navbar');
//   });
// }
window.$ = window.jQuery = require('jquery');
window.slick = require('slick-carousel');

require('./modules/browserDetect');
require('bootstrap-hover-dropdown');
require('./bootstrap/transition');
require('./bootstrap/collapse');
require('./bootstrap/tab');

import '../sass/main.scss';

const imagesContext = require.context('images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

if(NODE_ENV === 'development') {
  require('../views/pages/project.twig');
}


(function ($) {
  $(document).ready(function () {

  });

  $(window).on('load', function () {
    require('./modules/header');
    require('./modules/slider');
    require('./modules/planHome');

    require('../vue/simpleMap');
  });
})(jQuery);
