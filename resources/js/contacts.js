// if(module.hot) {
//   module.hot.accept('./common/navbar', function () {
//     require('./common/navbar');
//   });
// }
window.$ = window.jQuery = require('jquery');
window.slick = require('slick-carousel');

require('./modules/browserDetect');
require('bootstrap-hover-dropdown');
require('./bootstrap/collapse');
require('./bootstrap/tab');

import mapLoad from './modules/mapInit';
import loadScript from './modules/loadScripts';

const imagesContext = require.context('images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

import '../sass/main.scss';

if(NODE_ENV === 'development') {
  require('../views/pages/contacts.twig');
}


(function ($) {
  $(document).ready(function () {

  });

  $(window).on('load', function () {
    require('./modules/header');
    require('./modules/slider');

    loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;loadByRequire=1", function() {
      mapLoad();
    });
  });
})(jQuery);
