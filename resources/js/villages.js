// if(module.hot) {
//   module.hot.accept('./common/navbar', function () {
//     require('./common/navbar');
//   });
// }
window.$ = window.jQuery = require('jquery');
window.slick = require('slick-carousel');
// window.selectize = require('selectize');

import { svg } from './utils/svg';
require('./modules/browserDetect');
require('bootstrap-hover-dropdown');
// require('./components/modal');
require('./bootstrap/transition');
require('./bootstrap/collapse');
// require('./bootstrap/dropdown');
require('./bootstrap/tab');



const imagesContext = require.context('images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

import '../sass/main.scss';

if(NODE_ENV === 'development') {
  require('../views/pages/villages.twig');
}


(function ($) {
  $(document).ready(function () {

  });

  $(window).on('load', function () {
    require('./modules/header');
    require('./modules/slider');

    require('../vue/map-villages.js');
  });
})(jQuery);
