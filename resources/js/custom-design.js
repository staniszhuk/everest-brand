// if(module.hot) {
//   module.hot.accept('./common/navbar', function () {
//     require('./common/navbar');
//   });
// }
// window.$ = window.jQuery = require('jquery');
// window.slick = require('slick-carousel');

require('./modules/browserDetect');
require('bootstrap-hover-dropdown');
require('./bootstrap/transition');
require('./bootstrap/collapse');
require('./bootstrap/tab');


const imagesContext = require.context('images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

import '../sass/main.scss';

if(NODE_ENV === 'development') {
  require('../views/pages/custom-design.twig');
}


(function ($) {
  $(document).ready(function () {

  });

  $(window).on('load', function () {
    require('./modules/header');

    $( function() {
      $( "#slider-range" ).slider({
        range: true,
        min: 2000000,
        max: 10500000,
        values: [ 2200000, 10500000 ],
        step: 50000,
        slide: function( event, ui ) {
          console.log(ui.values);
          $( "#amount-min" ).val( ui.values[ 0 ] );
          $( "#amount-max" ).val( ui.values[ 1 ] );
        }
      });
      $( "#amount-min" ).val( $( "#slider-range" ).slider( "values", 0 ) );
      $( "#amount-max" ).val( $( "#slider-range" ).slider( "values", 1 ) );
    } );
  });
})(jQuery);
