import { svg } from '../../utils/svg';

let sliderHeight = window.innerHeight - $('header').height();
$('#slider-hero .slider__slide').height(sliderHeight);

$('#slider-hero').slick({
  infinite: true,
  slidesToShow: 1,
  speed: 300,
  dots: true
});

$('#slider-article-item').slick({
  infinite: true,
  slidesToShow: 1,
  speed: 300,
  dots: true
});

$('#slider-projects').slick({
  infinite: true,
  slidesToShow: 4,
  speed: 300,
  dots: true,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
});

$('#slider-already-seen').slick({
  infinite: true,
  slidesToShow: 4,
  speed: 300,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
});

$('.slider-history').slick({
  infinite: true,
  slidesToShow: 3,
  speed: 300,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        arrows: false,
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
      }
    }
  ]
});



// project card slider
 $('#slider-project').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  dots: true,
  asNavFor: '#slider-project-nav'
});
$('#slider-project-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '#slider-project',
  dots: true,
  focusOnSelect: true
});


// project card slider
 $('#slider-build').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  asNavFor: '#slider-build-nav'
});
$('#slider-build-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '#slider-build',
  dots: true,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 960,
      settings: {
        slidesToShow: 3,
        arrows: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        arrows: false
      }
    }
  ]
});

// slider nav append custom icons
$('.slick-next').html(svg({"name": "slider-next"}));
$('.slick-prev').html(svg({"name": "slider-prev"}));

