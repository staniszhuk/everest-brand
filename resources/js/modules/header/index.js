    let $window = $(window);
    let $header = $('header');
    let headerHeight = $header.height();

    $('body').css({ "padding-top": headerHeight });

    $window.resize(function() {
      if (headerHeight !== $header.height()) {
        headerHeight = $header.height();
        $('body').css({ "padding-top": headerHeight });
      }

      $('#slider-hero .slider__slide').height(window.innerHeight - headerHeight);

      // header resize
      if (window.innerWidth >= 992) {
        let $navbarMenu = $('#navbarMenu');
        let $navbarToggle = $('.js-navbar-toggle');

        if ($navbarMenu.hasClass('in') && !$navbarToggle.hasClass('collapsed'))
          $navbarMenu.removeClass('in');
          $navbarToggle.addClass('collapsed');
          $navbarToggle.addClass('collapsed');
          $('body').removeClass('nav-open')
      } 
    });

    $('body').click(function(e) {
      let $this = $(this);

      if (e.target.classList.contains('js-navbar-toggle') && e.target.classList.contains('collapsed') ) {
        $this.addClass('nav-open');
      } else $this.removeClass('nav-open')
    });