// import {
//   initMap,
//   addMarker
// } from "../googleMap";

// const initContactsMap = (center) => {
//   if ($('#contactsMap').length !== 0) {
//     let map = initMap({
//       center: center,
//       target: 'contactsMap',
//       zoom: 15
//     });

//     let marker = addMarker({
//       location: {
//         lat: center.lat,
//         lng: center.lng
//       },
//       target: map,
//       markerSize: new google.maps.Size(30, 40),
//       markerImg: '/assets/images/baloon.png'
//     });


//     // let resizeEvt
//     // $(window).bind('resize.gmap', function () {
//     //   clearTimeout(resizeEvt);
//     //   resizeEvt = setTimeout((function () {
//     //     google.maps.event.trigger(map, 'resize');
//     //     map.setCenter(location);
//     //   }), 500);
//     // });

//     return map;
//   }
// };

// const getCenter = (selector) => {
//   return {
//     lat: parseFloat(selector.attr('data-lat')),
//     lng: parseFloat(selector.attr('data-long'))
//   }
// };

// let coords = {
//   lat: 30.20689,
//   lng: -81.58217
// };

// initContactsMap(coords);




// // $('.js-contacts-offices').each(function () {

// //   const $toggles= $(this).find('.js-contacts-toggle');

// //   const $activeMap = $($toggles.filter('.active')[0]);

// //   let map;

// //   if($activeMap !== undefined) {
// //     const center = getCenter($activeMap);
// //     map = initContactsMap(center);
// //   }


// //   $toggles.each(function () {
// //     const $this = $(this);

// //     const center = getCenter($this);

// //     let marker = addMarker({
// //       location: {
// //         lat: center.lat,
// //         lng: center.lng
// //       },
// //       target: map,
// //       markerSize: new google.maps.Size(51, 61),
// //       markerImg: 'assets/images/mark.png'
// //     });

// //   });

// //   $toggles.on('click', function () {

// //     const $self = $(this);

// //     const center = getCenter($self);

// //     $toggles.removeClass('active');
// //     $self.addClass('active');

// //     let target = $self.attr('href');

// //     if (target === undefined) {
// //       return false;
// //     }

// //     map.panTo(center);

// //     target = target.substr(1);

// //     $('.js-contacts-target').each(function () {
// //       const $this = $(this);

// //       if($this.attr('data-contact') === target) {
// //         $this.removeClass('hidden');
// //       } else {
// //         $this.addClass('hidden');
// //       }
// //     });
// //   });


// // });

export default function mapLoad () {
  var myMap,
      myPlacemark,
      mapCoord = [57.624058, 39.893706],
      mapOption = {
        center: mapCoord,
        zoom: 16,
        controls: [ "zoomControl"]
      },
      mapPlacemarOptions = {
        // Чтобы балун и хинт открывались на метке, необходимо задать ей определенные свойства.
        balloonContentHeader: "Главный офис в Яросавле",
        balloonContentBody: "ул. Революционная, д.18, офис 200",
        hintContent: "ООО Эверест"
      }  

  if (window.map) {
      ymaps.ready(init);
  } else {
      setTimeout(mapLoad, 1000);
  }

  $('.tab__nav').delegate('a','click', function() {
    var $this = $(this);

    if (!$this.parent().hasClass('active')) {
      myMap.destroy();

      var coords = $this.attr('data-coord').split(',');

      mapCoord = [+coords[0], +coords[1]];
      mapOption.center = mapCoord;
      mapPlacemarOptions.balloonContentHeader = `Главный офис ${$this.attr('data-header')}`;
      mapPlacemarOptions.balloonContentBody = $this.attr('data-body');

      ymaps.ready(init);
    }
  });

  function init() {
    myMap = new ymaps.Map("map", mapOption),

    myPlacemark = new ymaps.Placemark(mapCoord, mapPlacemarOptions);
    myMap.geoObjects.add(myPlacemark);
    myMap.behaviors.disable('scrollZoom');
    myMap.behaviors.disable('drag');
  }

}