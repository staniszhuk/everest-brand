let DATACALC = {
	userSquare: null,
	floors: null,
	fundament: null,
	add: 0,
	walls: null,
	facing: null,
	masonry: null,
	roof: null,
	ceil: null,
	square: {
		roof: null,
		fundament: null,
		facing: null,
	},
	coast: {
		roof: null,
		fundament: null,
		facing: null,
	},
};

const COAST = {
	roof: [
		// один этаж
		[
			3800,
			3500,
			3100,
			2750,
		],
		// два этажа
		[
			3650,
			3300,
			2950,
			2600,
		]
	],
	facing: {
		mansarda: {
			balka: {
				oblicovka: {
					o88: {
						k440: 7250,
						g375: 5000,
						g400: 5380,
					},
					b88: {
						k440: 8150,
						g375: 5600,
						g400: 6050,
					},
					o65: {
						k440: 7975,
						g375: 5500,
						g400: 6809,
					},
					b65: {
						k440: 8150,
						g375: 6160,
						g400: 6700,
					},
				},
				nooblicovka: {
					k440: {
						no: 5805,
						sh: 6900, 
						shut: 7155,
					},
					g375: {
						no: 4150,
						sh: 4900, 
						shut: 5100, 
					},
					g400: {
						no: 4300,
						sh: 5100, 
						shut: 5300, 
					},
				}
			},
			plita: {
				oblicovka: {
					o88: {
						k440: 8500,
						g375: 6250,
						g400: 6630,
					},
					b88: {
						k440: 9400,
						g375: 6850,
						g400: 7300,
					},
					o65: {
						k440: 9225,
						g375: 6750,
						g400: 8059,
					},
					b65: {
						k440: 9400,
						g375: 7410,
						g400: 7950,
					},
				},
				nooblicovka: {
					k440: {
						no: 7100,
						sh: 8380, 
						shut: 9090,
					},
					g375: {
						no: 5400,
						sh: 6150, 
						shut: 6350,
					},
					g400: {
						no: 5550,
						sh: 6350, 
						shut: 6550,
					},
				}
			}
		},
		full: {
			balka: {
				oblicovka: {
					o88: {
						k440: 8350,
						g375: 5750,
						g400: 6190,
					},
					b88: {
						k440: 9300,
						g375: 6400,
						g400: 6900,
					},
					o65: {
						k440: 9190,
						g375: 6325,
						g400: 6809,
					},
					b65: {
						k440: 10200,
						g375: 7050,
						g400: 7600,
					},
				},
				nooblicovka: {
					k440: {
						no: 6676,
						sh: 7935, 
						shut: 8288,
					},
					g375: {
						no: 4750,
						sh: 5600, 
						shut: 5800, 
					},
					g400: {
						no: 4750,
						sh: 5800, 
						shut: 6000, 
					},
				}
			},
			plita: {
				oblicovka: {
					o88: {
						k440: 9600,
						g375: 7000,
						g400: 7440,
					},
					b88: {
						k440: 10550,
						g375: 7650,
						g400: 8150,
					},
					o65: {
						k440: 10440,
						g375: 7575,
						g400: 8059,
					},
					b65: {
						k440: 11450,
						g375: 8300,
						g400: 8850,
					}
				},
				nooblicovka: {
					k440: {
						no: 7926,
						sh: 9185, 
						shut: 9478,
					},
					g375: {
						no: 6000,
						sh: 6850, 
						shut: 7050, 
					},
					g400: {
						no: 6000,
						sh: 7050, 
						shut: 7250, 
					},
				}
			}
		},
	},
};


const calcForm = document.forms.calculator;
let resultOutput = document.getElementById('js-calculator-result');
let inputFull = calcForm.elements.full;

$('select').not('#facing').selectize();
$('#facing').selectize({
	onChange(value) {
		const facingWorkInput = calcForm.elements['facing-work'];

		if (+value === 0) {
			facingWorkInput.parentElement.classList.remove('calculator__item-hidden')
		} else {
			facingWorkInput.parentElement.classList.add('calculator__item-hidden')
		}
	}
});

calcForm.elements.oneFloor.addEventListener('click', function() {
	inputFull.disabled = true;

	if (!calcForm.elements.mansarda.checked) calcForm.elements.mansarda.checked = true;
})

calcForm.elements.twoFloor.addEventListener('click', function() {
	inputFull.disabled = false;
});


calcForm.addEventListener('submit', function (e) {
	e.preventDefault();

	const data = new FormData(calculator);
	let validation = null;

	for (const entry of data) {
		if (validation === null) {
			if (entry[1] === '') {
				resultOutput.innerHTML = 'Для расчета надо указать все параметры';
				validation = entry[0];

				//можно здесь отвалидировать конкретое поле 
				return;
			}
		}
	};

	// записываем данные из формы в объект
	for (const entry of data) {
		if (isNumeric(entry[1])) DATACALC[entry[0]] = +entry[1];
		else DATACALC[entry[0]] = entry[1]
	};

	function isNumeric(num) {
		return !isNaN(num)
	}

	function calculateFundament() {
		 // с облицовки
		if (+DATACALC.floors === 1) DATACALC.square.fundament = DATACALC.userSquare * 1.4; // один этаж
		else DATACALC.square.fundament = DATACALC.userSquare * 0.76; // два этажа

		// без облицовки
		if (DATACALC.facing === 1) DATACALC.square.fundament /= 1.04;

		DATACALC.coast.fundament = Math.floor(DATACALC.square.fundament * 4100); // цена фундамента
	}

	function calculateRoof() {
		 // с облицовки
		if (DATACALC.facing !== 0) {
			if (DATACALC.floors === 1) DATACALC.square.roof = DATACALC.userSquare * 1.4 * 1.45; // один этаж
			else DATACALC.square.roof = DATACALC.square.fundament * 1.5; // два этажа
		} else { // без облицовки
			if (DATACALC.floors === 1) DATACALC.square.roof = DATACALC.square.fundament * 1.45 / 1.03; // один этаж
			else DATACALC.square.roof = DATACALC.square.fundament * 1.5 / 1.03; // два этажа
		}

		DATACALC.coast.roof = Math.floor(DATACALC.square.roof * COAST.roof[DATACALC.floors - 1][DATACALC.roof]); // цена фундамента
	}

	function calculateFacing() {
		let facing,
			floors;

		if (DATACALC.floors === 1) floors = 'mansarda' // один этаж
		else floors = 'full' // два этажа

		 // с облицовки
		if (DATACALC.facing === 0) {
			facing = 'nooblicovka';

			DATACALC.coast.facing = Math.floor(DATACALC.userSquare * COAST.facing[floors][DATACALC.ceil][facing][DATACALC.walls][DATACALC['facing-work']]);
		} else {
			facing = 'oblicovka';

			DATACALC.coast.facing = Math.floor(DATACALC.userSquare * COAST.facing[floors][DATACALC.ceil][facing][DATACALC.facing][DATACALC.walls]);
		}
	}

	function calcFull() {
		if (DATACALC.userSquare < 50) {
			DATACALC.coast.full *= 1.5; 
		} else if (DATACALC.userSquare >= 50 && DATACALC.userSquare < 100) {
			DATACALC.coast.full += (900000 * 3);
		} else if (DATACALC.userSquare >= 100 && DATACALC.userSquare < 150) {
			DATACALC.coast.full = DATACALC.coast.roof + 1100000 + DATACALC.coast.fundament * 1.7 + DATACALC.coast.facing * 1.66; 
		} else if (DATACALC.userSquare >= 150 && DATACALC.userSquare < 200) {
			DATACALC.coast.full = DATACALC.coast.roof * 1.55 + DATACALC.coast.fundament * 1.66 + DATACALC.coast.facing * 1.8; 
		} else if (DATACALC.userSquare >= 200 && DATACALC.userSquare < 250) {
			DATACALC.coast.full += (1100000 * 3);
		} else if (DATACALC.userSquare >= 250 && DATACALC.userSquare < 300) {
			DATACALC.coast.full += (1100000 * 3);
		} else {
			DATACALC.coast.full += (1100000 * 3);
		}
	}

	calculateFundament();
	calculateRoof();
	calculateFacing();
	calcFull();

	DATACALC.coast.full = DATACALC.coast.roof + DATACALC.coast.fundament + DATACALC.coast.facing; 
	
	console.log('Кровля', DATACALC.coast.roof);
	console.log('Фундамент:', DATACALC.coast.fundament);
	console.log('Перекрытия/стены:', DATACALC.coast.facing);
	console.log('Полная:', DATACALC.coast.full);

	resultOutput.innerHTML = DATACALC.coast.full + ' ₽';
});