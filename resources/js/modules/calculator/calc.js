window.addEventListener('DOMContentLoaded', () => {
	let DATACALC = {
		square: null,
		squareFull: null,
		type: null,
		mansarda: null,
		overCoast: null,
		facade: {
			name: null,
			work: null,
		},
		fundament: {
			name: null,
			coast: null,
			square: null,
		},
		masonry: {
			coast: null
		},
		roof: {
			name: null,
			coast: null,
			square: null,
		}
	};

	const COAST = {
		coff: {
			mansarda: {
				on: 0.76,
				off: 1.33,
			},
			roof: {
				off: 1.43,
				on: 1.1,
				paintingOn: 0.95,
			}
		},
		fundament: {
			facadeOn: {
				FSR: 4130,
				FMPOC: 5550,
				FMP: 4500,
				FLS: 11450,
				FLSPP: 14700,
			},
			facadeOff: {
				FSR: 3950,
				FMPOC: 5300,
				FMP: 4300,
				FLS: 11000,
				FLSPP: 14200,
			}
		},
		masonry: {
			facadeOff: {
				block375: {
					DB: {
						off: 3900,
						O: 4600,
						OY: 4895,
						OO: 5000,
						OOY: 5500,
					},
					PSGB: {
						off: 4950,
						O: 5840,
						OY: 6170,
						OO: 6350,
						OOY: 6985,
					}
				},
				block400: {
					DB: {
						off: 4060,
						O: 4780,
						OY: 5090,
						OO: 5200,
						OOY: 5720,
					},
					PSGB: {
						off: 5145,
						O: 6070,
						OY: 6415,
						OO: 6600,
						OOY: 7260,
					}
				},
				ceramic: {
					DB: {
						off: 5630,
						O: 6650,
						OY: 7095,
						OO: 7210,
						OOY: 7938,
					},
					PSGB: {
						off: 7100,
						O: 8380,
						OY: 8940,
						OO: 9090,
						OOY: 10000,
					}
				},
				brick: {
					// в разработке
				}
			},
			facadeOn: {
				OK65: {
					DB: {
						block375: 6450,
						block400: 6708,
						ceramic: 8250,
					},
					PSGB: {
						block375: 7750,
						block400: 8110,
						ceramic: 9700,
					}
				},
				OK65B: {
					DB: {
						block375: 7050,
						block400: 7330,
						ceramic: 7600,
					},
					PSGB: {
						block375: 8450,
						block400: 8780,
						ceramic: 9120,
					}
				},
				YK88: {
					DB: {
						block375: 6250,
						block400: 6500,
						ceramic: 7950,
					},
					PSGB: {
						block375: 7500,
						block400: 7800,
						ceramic: 9350,
					}
				},
				YK88B: {
					DB: {
						block375: 6750,
						block400: 7020,
						ceramic: 7300,
					},
					PSGB: {
						block375: 8150,
						block400: 8470,
						ceramic: 8750,
					}
				}
			}
		},
		roof: {
			MY: 2950,
			M: 2590,
			SOFTY: 3650,
			SOFT: 3300,
		}
	};

	const form = document.getElementById('form');
	const stageData = document.getElementById('stage-data');
	const stageFundament = document.getElementById('stage-fundament');
	const stageDataHidden = document.getElementById('stage-data-hidden');
	const finish = form.querySelector('#finish');
	const result = form.querySelector('#result');
	const fullSquareResult = document.querySelector('.fullSquareResult');

	/* Эементы пощади */
	const squares = Array.prototype.slice.call(document.querySelectorAll('input[data-action="calc-square"]')),
		squareHeight = form.elements.HEIGHT,
		squareWidth = form.elements.WIDTH,
		squareFull = form.elements.SQUARE;

	/* Селект 'Тип исполнения дома' */
	const homeType = form.elements.HOMETYPE;
	/* Селект 'Тип фасада' */
	const homeHeight = document.getElementById('home-height');
	/* Селект 'Тип этажность' */
	const homeFacadeType = document.getElementById('home-facade-type');
	/* Селект 'Тип фундамент' */
	const homeWalls = document.getElementById('home-walls');
	/* Селект 'Тип фундамент' */
	const homePainting = document.getElementById('home-painting');
	/* Селект 'Тип фундамент' */
	const homeCeiling = document.getElementById('home-ceiling');
	/* Селект 'Тип фундамент' */
	const homeFundament = document.getElementById('home-fundament');
	/* Селект 'Тип фундамент' */
	const homeRoof = document.getElementById('home-roof');
	/* блоки стадий' */
	const stages = Array.prototype.slice.call(document.querySelectorAll('div[data-stage]'));

	// События для инпутов площадей
	squares.forEach(square => {
		square.addEventListener('keyup', (e) => {
			const target = e.target;
			if (target.value.length > 3) e.target.value = e.target.value.substring(0, target.value.length - 1);
			if (target.value > +e.target.getAttribute('max')) target.value = target.getAttribute('max');

			let height = squareHeight.value;
			let width = squareWidth.value;

			if (isNumeric(height) && isNumeric(width)) {
				squareFull.value = width * height;
				DATACALC.overCoast = calcOverCoast(squareFull.value);

			} else squareFull.value = 0;

			DATACALC.square = +squareFull.value;

			if (DATACALC.mansarda) {
				fullSquareCalc()
			}

			submitCheck();

			if (DATACALC.mansarda && DATACALC.facade.name && DATACALC.square) {
				stageFundament.classList.add('active');
			}
		});
	});

	squareFull.addEventListener('keyup', (e) => {
		if (e.target.value.length > 6) e.target.value = 100000;

		if (isNumeric(e.target.value)) {
			DATACALC.square = squareFull.value;
			DATACALC.overCoast = calcOverCoast(squareFull.value);
		} else squareFull.value = '';

		if (DATACALC.mansarda) {
			fullSquareCalc()
		}

		if (DATACALC.mansarda && DATACALC.facade.name && DATACALC.square) {
			stageFundament.classList.add('active');
		}

		squares.forEach(square => {
			square.value = '';
		});

	})

	// События для селекта типа дома
	homeType.addEventListener('change', (e) => {
		const target = e.target;

		stages.forEach((stage) => {
			if (target.value === stage.getAttribute('data-stage')) {
				if (stageData.children.length > 0) {
					stageDataHidden.appendChild(stageData.children[0]);
				}

				stageData.appendChild(stage);
			}
		});

		if (DATACALC.mansarda && DATACALC.facade.name && DATACALC.square) {
			stageFundament.classList.add('active');
		}

		DATACALC.type = target.value;
		submitCheck();
	});

	// События для селекта высоты дома
	homeHeight.addEventListener('change', (e) => {
		DATACALC.mansarda = e.target.value;

		if (DATACALC.mansarda && DATACALC.facade.name && DATACALC.square) {
			stageFundament.classList.add('active');
		}

		if (DATACALC.square) {
			fullSquareCalc();
		}

		submitCheck();
	});

	// События для селекта фасада
	homeFacadeType.addEventListener('change', (e) => {
		DATACALC.facade.name = e.target.value;

		if (DATACALC.facade.name === 'off') {
			homePainting.parentElement.classList.add('active')
			DATACALC.facade.work = 'facadeOff';
		} else {
			homePainting.parentElement.classList.remove('active');
			DATACALC.facade.work = 'facadeOn';
		}

		if (DATACALC.mansarda && DATACALC.facade.name && DATACALC.square) {
			stageFundament.classList.add('active');
		}

		submitCheck();
	});



	// События для селекта фундамент
	homeFundament.addEventListener('change', (e) => {
		DATACALC.fundament.name = e.target.value;

		DATACALC.fundament.coast = +COAST.fundament[DATACALC.facade.work][DATACALC.fundament.name] * +COAST.coff.mansarda[DATACALC.mansarda] * DATACALC.squareFull;

		submitCheck();
	});

	// События для селекта стены
	homeWalls.addEventListener('change', (e) => {
		DATACALC.walls = e.target.value;

		submitCheck();
	});

	// События для селекта перекрытия
	homeCeiling.addEventListener('change', (e) => {
		DATACALC.ceiling = e.target.value;

		submitCheck();
	});

	// События для селекта отделка
	homePainting.addEventListener('change', (e) => {
		DATACALC.painting = e.target.value;

		submitCheck();
	});

	// События для селекта крыши
	homeRoof.addEventListener('change', (e) => {
		DATACALC.roof.name = e.target.value;

		submitCheck();
	});

	finish.addEventListener('click', (e) => {
		calcFundamentSquare();
		calcRoofSquare();
		calcRoofCoast();
		calcMasonryCoast();

		let fullCoast = +DATACALC.roof.coast + +DATACALC.masonry.coast + +DATACALC.fundament.coast;

		let roofPercent = DATACALC.roof.coast / fullCoast * 100;
		let FundamentPercent = DATACALC.fundament.coast / fullCoast * 100;
		let MasonryPercent = DATACALC.masonry.coast / fullCoast * 100;

		let fullCoastPlus = DATACALC.overCoast === 1.5 ? fullCoast * DATACALC.overCoast : fullCoast + DATACALC.overCoast;

		let coastRoofPlus = (roofPercent * fullCoastPlus / 100).toFixed(0);
		let coastFundamentPlus = (FundamentPercent * fullCoastPlus / 100).toFixed(0);
		let coastMasonryPlus = (MasonryPercent * fullCoastPlus / 100).toFixed(0);

		console.log(roofPercent, FundamentPercent, MasonryPercent);

		result.innerHTML = `
	            <div>Стоимость Фундамента: ${DATACALC.fundament.coast}</div>
	            <div>Стоимость Кладки: ${DATACALC.masonry.coast}</div>
	            <div>Стоимость Крыши: ${DATACALC.roof.coast}</div>
	            <br/>
	            <div>Общая стоимость: ${fullCoast}</div>
	            <div>+</div>
	            <div>Надбавка по площади: ${DATACALC.overCoast === 1.5 ? 'X' : '+'}${DATACALC.overCoast}</div>
	            <div>=</div>
	            <div>Общая стоимость с надбавкой: ${fullCoastPlus}</div>
	            <br/>
	            <div>Итоговая стоимость Фундамента: ${coastFundamentPlus}</div>
	            <div>Итоговая стоимость Кладки: ${coastMasonryPlus}</div>
	            <div>Итоговая стоимость Крыши: ${coastRoofPlus}</div>
	        `;

		console.log(DATACALC);
	});

	function submitCheck() {
		if (DATACALC.ceiling && DATACALC.facade.work && DATACALC.mansarda && DATACALC.roof.name && DATACALC.squareFull && DATACALC.walls && DATACALC.type) finish.classList.add('active');
		else finish.classList.remove('active');
	}

	function calcOverCoast(square) {
		if (square < 55) {
			return 1.5;
		} else if (square >= 55 && square < 105) {
			return 900000;
		} else if (square >= 105) {
			return 1100000;
		}
	}

	function calcMasonryCoast() {
		if (DATACALC.facade.work === 'facadeOff') DATACALC.masonry.coast = COAST.masonry[DATACALC.facade.work][DATACALC.walls][DATACALC.ceiling][DATACALC.painting] * DATACALC.squareFull;

		else DATACALC.masonry.coast = +(COAST.masonry[DATACALC.facade.work][DATACALC.facade.name][DATACALC.ceiling][DATACALC.walls] * DATACALC.squareFull);
	}

	function calcFundamentSquare() {
		DATACALC.fundament.square = +(DATACALC.squareFull * COAST.coff.mansarda[DATACALC.mansarda]).toFixed(1);
	}

	function calcRoofSquare() {
		if (DATACALC.mansarda === 'off' && DATACALC.facade.work === 'facadeOn') {
			DATACALC.roof.square = DATACALC.fundament.square * COAST.coff.roof[DATACALC.mansarda];
		} else if (DATACALC.mansarda === 'on' && DATACALC.facade.work === 'facadeOn') {
			DATACALC.roof.square = DATACALC.squareFull * COAST.coff.roof[DATACALC.mansarda];
		} else if (DATACALC.mansarda === 'off' && DATACALC.facade.work === 'facadeOff') {
			DATACALC.roof.square = DATACALC.fundament.square * COAST.coff.roof[DATACALC.mansarda] * COAST.coff.roof.paintingOn;
		} else if (DATACALC.mansarda === 'on' && DATACALC.facade.work === 'facadeOff') {
			DATACALC.roof.square = DATACALC.squareFull * COAST.coff.roof[DATACALC.mansarda] * COAST.coff.roof.paintingOn;
		}

		DATACALC.roof.square = +DATACALC.roof.square.toFixed(1);
	}

	function calcRoofCoast() {
		DATACALC.roof.coast = +(DATACALC.roof.square * COAST.roof[DATACALC.roof.name]).toFixed(1);
	}

	// проверка на целое число
	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n) && n > 0;
	}

	// полная площадь работ
	function fullSquareCalc() {
		if (DATACALC.mansarda === 'on') {
			DATACALC.squareFull = DATACALC.square * 2;
		} else DATACALC.squareFull = DATACALC.square;

		fullSquareResult.innerHTML = 'Полная площадь работ: ' + DATACALC.squareFull;
	}
});