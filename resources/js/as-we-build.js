// if(module.hot) {
//   module.hot.accept('./common/navbar', function () {
//     require('./common/navbar');
//   });
// }
window.$ = window.jQuery = require('jquery');
window.slick = require('slick-carousel');
window.magnificPopup = require('magnific-popup');
// window.selectize = require('selectize');

require('./modules/browserDetect');
require('bootstrap-hover-dropdown');
require('./bootstrap/transition');
require('./bootstrap/collapse');
require('./bootstrap/tab');


const imagesContext = require.context('images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

import '../sass/main.scss';

if(NODE_ENV === 'development') {
  require('../views/pages/as-we-build.twig');
}


(function ($) {
  $(document).ready(function () {

  });

  $(window).on('load', function () {
    require('./modules/header');
    require('./modules/slider');
    require('./modules/zoomGallery');

  });
})(jQuery);
