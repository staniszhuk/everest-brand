// if(module.hot) {
//   module.hot.accept('./common/navbar', function () {
//     require('./common/navbar');
//   });
// }
window.$ = window.jQuery = require('jquery');
window.slick = require('slick-carousel');
window.skrollr = require('skrollr');
window.magnificPopup = require('magnific-popup');
window.slider = require('jquery-ui/ui/widgets/slider');
window.selectize = require('selectize');



require('./modules/browserDetect');
require('bootstrap-hover-dropdown');
require('./components/modal');
require('./bootstrap/transition');
require('./bootstrap/collapse');
// require('./bootstrap/dropdown');
require('./bootstrap/tab');

import mapLoad from './modules/mapInit';
import loadScript from './modules/loadScripts';


const imagesContext = require.context('images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

import '../sass/main.scss';

if(NODE_ENV === 'development') {
  require('../views/pages/calculator.twig');
}


(function ($) {
  $(document).ready(function () {
  });

  $(window).on('load', function () {

    require('./modules/header');
    require('./modules/slider');

    if (window.location.pathname === '/') {
      var s = skrollr.init({});
    }

    if (window.location.pathname === '/as-we-build.html') {
      require('./modules/zoomGallery');
    }

    if (window.location.pathname === '/project.html') {
      require('./modules/planHome');
      require('../vue/simpleMap');
    }

    if (window.location.pathname === '/contacts.html') {
      loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;loadByRequire=1", function() {
        mapLoad();
      });
    }

    if (window.location.pathname === '/company.html') {
      const $sliderRange = $('.slider-range');
      const sliderRangeNumber = 6;
      let sliderRangeActive = 0;

      $( function() {
        $("#slider-range").slider({
          range: 'min',
          animate: "slow",
          min: 0,
          max: sliderRangeNumber - 1,
          change: function( event, ui ) {
            let activeTab = `[data-range='${sliderRangeActive}']`
            let nextTab = `[data-range='${ui.value}']`

            $sliderRange.find(activeTab).removeClass(`active`);
            $sliderRange.find(nextTab).addClass(`active`);

            sliderRangeActive = ui.value;
          }
        });
      });
    }

    if (window.location.pathname === '/custom-design.html' || window.location.pathname === '/project-map.html') {
      $( function() {
        $( "#slider-range" ).slider({
          range: true,
          min: 2000000,
          max: 10500000,
          values: [ 2200000, 10500000 ],
          step: 50000,
          slide: function( event, ui ) {
            $( "#amount-min" ).val( ui.values[ 0 ] );
            $( "#amount-max" ).val( ui.values[ 1 ] );
          }
        });
        $( "#amount-min" ).val( $( "#slider-range" ).slider( "values", 0 ) );
        $( "#amount-max" ).val( $( "#slider-range" ).slider( "values", 1 ) );
      });
    }

    if (window.location.pathname === '/projects-catalog.html') {
      $( function() {
        $( "#slider-range" ).slider({
          range: true,
          min: 2000000,
          max: 10500000,
          values: [ 3200000, 10500000 ],
          step: 50000,
          slide: function( event, ui ) {
            $( "#amount-min" ).val( ui.values[ 0 ] );
            $( "#amount-max" ).val( ui.values[ 1 ] );
          }
        });
        $( "#amount-min" ).val( $( "#slider-range" ).slider( "values", 0 ) );
        $( "#amount-max" ).val( $( "#slider-range" ).slider( "values", 1 ) );
      });

      $( function() {
        $( "#slider-range-2" ).slider({
          range: true,
          min: 50,
          max: 500,
          values: [ 55, 450 ],
          step: 5,
          slide: function( event, ui ) {
            $( "#amount-min-2" ).val( ui.values[ 0 ] );
            $( "#amount-max-2" ).val( ui.values[ 1 ] );
          }
        });
        $( "#amount-min-2" ).val( $( "#slider-range-2" ).slider( "values", 0 ) );
        $( "#amount-max-2" ).val( $( "#slider-range-2" ).slider( "values", 1 ) );
      });
    }

    if (window.location.pathname === '/houses270.html') {
      require('../vue/app.js');
    }

    if (window.location.pathname === '/ready-and-build.html') {
      require('./modules/planHome');
      require('../vue/app.js');
      require('../vue/map-second.js');
    }

    if (window.location.pathname === '/villages.html') {
      require('../vue/map-villages.js');
    }

    if (window.location.pathname === '/village.html') {
      require('./modules/zoomGallery');
      require('../vue/map-villages.js');
    }

    if (window.location.pathname === '/projects-catalog.html') {
      $('#projects-filters').selectize();
    }

    if (window.location.pathname === '/calculator.html') {
      $( function() {
        $( "#slider-range-square" ).slider({
          range: 'min',
          min: 50,
          max: 500,
          step: 5,
          slide: function( event, ui ) {
            $( "#userSquare" ).val( ui.value );
          }
        });
        $( "#userSquare" ).val( $( "#slider-range-square" ).slider( "values", 0 ) );
      });
      
      require('./modules/calculator');
    }


    $('.respons-video__preview').on('click', function(e) {
      const iframeWrap = e.target.nextElementSibling;
      const videoSrc = iframeWrap.getAttribute('data-src');

      iframeWrap.querySelector('iframe').src = videoSrc;
      $(this).remove();
    });
  });
})(jQuery);
