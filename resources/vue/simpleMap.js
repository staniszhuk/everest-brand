import Vue from 'vue';
import simpleMap from 'vue-components/simpleMap';
import YmapPlugin  from "vue-yandex-maps";


Vue.use(YmapPlugin);

export default new Vue({
  el: '#simple-map',
  render: h => h(simpleMap),
});
