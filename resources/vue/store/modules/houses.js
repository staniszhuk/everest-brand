export default {
    state: {
        houses: [
            {
                id: 1,
                image: 'home-1.jpg',
                material: 'plaster',
                type: 'штукатурки',
                area: '204',
                start: '2 апреля 2018 г.',
                end: '20 сентября 2018 г.',
                href: '#home',
                city: 'yaroslavl',
                coords: [52.1, 39.1],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "1",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                icon: {
                    color: 'red',
                }
            },
            {
                id: 2,
                image: 'home-1.jpg',
                material: 'brick',
                type: 'кирпича',
                area: '154',
                start: '2 апреля 2018 г.',
                end: '20 сентября 2018 г.',
                href: '#home',
                city: 'yaroslavl',
                coords: [54.2, 139.2],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "2",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                icon: {
                    color: 'green',
                }
            },
            {
                id: 3,
                image: 'home-1.jpg',
                material: 'plaster',
                type: 'штукатурки',
                area: '174',
                start: '2 апреля 2018 г.',
                end: '20 сентября 2018 г.',
                href: '#home',
                city: 'yaroslavl',
                coords: [51.6, 39.3],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "3",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                icon: {
                    color: 'red',
                }
            },
            {
                id: 4,
                image: 'home-1.jpg',
                material: 'wood',
                type: 'дерева',
                area: '124',
                start: '2 апреля 2018 г.',
                end: '20 сентября 2018 г.',
                href: '#home',
                city: 'yaroslavl',
                coords: [54.4, 39.9],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "4",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                icon: {
                    color: 'orange',
                }
            },
            {
                id: 5,
                image: 'home-1.jpg',
                material: 'plaster',
                type: 'штукатурки',
                area: '140',
                start: '2 апреля 2018 г.',
                end: '20 сентября 2018 г.',
                href: '#home',
                city: 'novgorod',
                coords: [54.5, 39.5],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "5",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                icon: {
                    color: 'red',
                }
            },
            {
                id: 6,
                image: 'home-1.jpg',
                material: 'brick',
                type: 'кирпича',
                area: '184',
                start: '2 апреля 2018 г.',
                end: '20 сентября 2018 г.',
                href: '#home',
                city: 'novgorod',
                coords: [54.6, 39.6],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "6",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                icon: {
                    color: 'green',
                }
            },
            {
                id: 7,
                image: 'home-1.jpg',
                material: 'wood',
                type: 'дерева',
                area: '138',
                start: '2 апреля 2018 г.',
                end: '20 сентября 2018 г.',
                href: '#home',
                city: 'moscow',
                coords: [54.7, 39.7],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "7",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                icon: {
                    color: 'blue',
                }
            },
            {
                id: 8,
                image: 'home-1.jpg',
                material: 'brick',
                type: 'кирпича',
                area: '145',
                start: '2 апреля 2018 г.',
                end: '20 сентября 2018 г.',
                href: '#home',
                city: 'moscow',
                coords: [54.8, 39.8],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "8",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                icon: {
                    color: 'red',
                }
            },
            {
                id: 9,
                image: 'home-1.jpg',
                material: 'wood',
                type: 'дерева',
                area: '168',
                start: '2 апреля 2018 г.',
                end: '20 сентября 2018 г.',
                href: '#home',
                city: 'moscow',
                coords: [54.9, 39.9],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "9",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                icon: {
                    color: 'blue',
                }
            },
        ],
        filteredHouses: [],
        center: [54.62896654088406, 39.731893822753904],
        loading: true,
    },
    mutations: {
        filterChange(state, payload) {
            switch (payload.type) {
                case 'square':
                    if (payload.filter === 'squareUp') {
                        state.filteredHouses = state.houses.sort((a, b) => {
                            if( a.area > b.area) return 1;
                            if( a.area < b.area) return -1;
                            return 0;
                        });
                    } else {
                        state.filteredHouses = state.houses.sort((a, b) => {
                            if( a.area > b.area) return -1;
                            if( a.area < b.area) return 1;
                            return 0;
                        });
                    }
                    break;
            
                case 'material':
                    state.filteredHouses = state.houses.filter((house) => {
                        return house[payload.type] === payload.filter;
                    });
                    break;

                case 'default':
                    state.filteredHouses = state.houses;
                    break;
            
                default:
                    state.filteredHouses = state.houses;
                    break;
            }
        },
        changeCenter(state, payload) {
            state.center = payload;
        },
        setLoading(state, payload) {
            state.loading = payload;
        },
    },
    getters: {
        house(state) {
            return houseId => state.houses.find(house => house.id === houseId)
        }
    },
    actions: {
        filterChange({ commit }, payload) {
            commit('filterChange', payload);
        },
        changeCenter({ commit }, payload) {
            commit('changeCenter', payload);
        },
        setLoading({ commit }, payload) {
            commit('setLoading', payload);
        }
    }
}