export default {
    state: {
        villages: [
            {
                id: 1,
                name: 'Название поселка',
                image: 'home-1.jpg',
                village: 'moscow',
                coords: [52.1, 39.1],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "1",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                stock: [
                    'готовые дома',
                    'коробки', 
                    'участки',
                ],
                icon: {
                    color: 'red',
                }
            },
            {
                id: 2,
                name: 'Название поселка',
                image: 'home-1.jpg',
                village: 'yaroslavl',
                coords: [54.2, 139.2],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "2",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                stock: [
                    'готовые дома',
                    'коробки', 
                    'участки',
                ],
                icon: {
                    color: 'green',
                }
            },
            {
                id: 3,
                name: 'Название поселка',
                image: 'home-1.jpg',
                village: 'yaroslavl',
                coords: [51.6, 39.3],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "3",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                stock: [
                    'готовые дома',
                    'коробки', 
                    'участки',
                ],
                icon: {
                    color: 'red',
                }
            },
            {
                id: 4,
                name: 'Название поселка',
                image: 'home-1.jpg',
                village: 'nnovgorod',
                coords: [54.4, 39.9],
                properties: {}, // define properties here
                options: {}, // define options here
                clusterName: "4",
                balloonTemplate: '<div>"Your custom template"</div>',
                callbacks: { click: function() {} },
                stock: [
                    'готовые дома',
                    'коробки', 
                    'участки',
                ],
                icon: {
                    color: 'orange',
                }
            },
        ],
        center: [54.62896654088406, 39.731893822753904],
        loading: true,
        filteredVillages: [],
    },
    mutations: {
        villageChange(state, payload) {
            state.filteredVillages = payload;
        },
        changeCenter(state, payload) {
            state.center = payload;
        },
        setLoading(state, payload) {
            state.loading = payload;
        },
    },
    getters: {
        village(state) {
            return villageId => state.villages.find(village => village.id === villageId)
        },
        villages(state) {
            return state.villages;
        }
    },
    actions: {
        villageChange({ commit, getters }, payload) {
            commit('setLoading', true);

            const filteredVillages = getters.villages.filter(village => {
                return village.village === payload;
            });

            commit('villageChange', filteredVillages);
            commit('setLoading', false);
        },
        changeCenter({ commit }, payload) {
            commit('changeCenter', payload);
        },
        setLoading({ commit }, payload) {
            commit('setLoading', payload);
        }
    }
}