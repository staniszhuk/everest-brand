import Vue from "vue";
import Vuex from "vuex";
import houses from "./modules/houses";
import villages from "./modules/villages";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        houses,
        villages,
    },
    state: {},
    getters: {},
});