import Vue from 'vue';
import MapVillages from 'vue-components/MapVillages';
import store from "./store";
import YmapPlugin  from "vue-yandex-maps";

Vue.use(YmapPlugin);

export default new Vue({
  el: '#app',
  render: h => h(MapVillages),
  store,
});
