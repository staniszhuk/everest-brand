import Vue from 'vue';
import App from 'vue-components/App';
import store from "./store";
import YmapPlugin  from "vue-yandex-maps";
import VueScrollTo from "vue-scrollto";


Vue.use(VueScrollTo, {
  container: "body",
  duration: 500,
  easing: "ease",
  offset: 0,
  cancelable: true,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
})

Vue.use(YmapPlugin);

export default new Vue({
  el: '#app-map',
  render: h => h(App),
  store,
});
