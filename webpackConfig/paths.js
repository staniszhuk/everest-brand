const path = require('path');

const paths =  {
  root: 'resources',
  public: 'public',
  clean: ['public']
};

module.exports = paths;
