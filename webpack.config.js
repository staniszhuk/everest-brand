const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const SpritesmithPlugin = require('webpack-spritesmith');

const isVue = true;
// if true, install packages: 'vue', 'vue-loader', 'vue-style-loader', 'vue-template-compiler'

const NODE_ENV = process.env.NODE_ENV || 'development';
const isProd = NODE_ENV === 'production';

const paths = require('./webpackConfig/paths');

//Vue
let vueAliases = {};
let vueRule = {};
let vuePlugins = [];

if(isVue) {
  const {VueLoaderPlugin} = require('vue-loader');

  vueRule = {
    test: /\.vue$/,
    use: {
      loader: "vue-loader"
    }
  };
  vueAliases = {
    'vue-components': path.resolve(paths.root, 'vue/components'),
    'vue-modules': path.resolve(paths.root, 'vue/modules'),
    'vue-utils': path.resolve(paths.root, 'vue/utils'),
    'vue$': 'vue/dist/vue.esm.js'
  };

  vuePlugins = [new VueLoaderPlugin()];
}
//end Vue

const devServer = {
  open: true,
  clientLogLevel: 'none',
  index: 'index.html',
  contentBase: path.resolve(paths.root),
};

let twigToHtml = [
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/index.twig'),
    filename: 'index.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/contacts.twig'),
    filename: 'contacts.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/blog.twig'),
    filename: 'blog.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/article.twig'),
    filename: 'blog-category.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/company.twig'),
    filename: 'company.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/documents.twig'),
    filename: 'documents.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/team.twig'),
    filename: 'team.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/houses270.twig'),
    filename: 'houses270.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/ready-and-build.twig'),
    filename: 'ready-and-build.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/project.twig'),
    filename: 'project.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/villages.twig'),
    filename: 'villages.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/village.twig'),
    filename: 'village.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/as-we-build.twig'),
    filename: 'as-we-build.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/custom-design.twig'),
    filename: 'custom-design.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/projects-catalog.twig'),
    filename: 'projects-catalog.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/projects-catalog-no-results.twig'),
    filename: 'projects-catalog-no-results.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/project-map.twig'),
    filename: 'project-map.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(paths.root, 'views/pages/calculator.twig'),
    filename: 'calculator.html',
    chunks: ['index'],
    templateParameters: {
      socials: require(path.resolve(paths.root, 'data/socials.json'))
    }
  }),
];

const imgRules = [
  {
    test: /\.svg$/,
    include: path.resolve(paths.root, 'assets/svg'),
    use: [
      {
        loader: "file-loader",
        options: {
          context: path.resolve(paths.root),
          name(file) {
            return isProd ? '[path][name].[ext]' : '[path][name].[hash].[ext]';
          }
        }
      }
    ]
  },
  {
    test: /\.(jpg|png|svg)$/,
    exclude: path.resolve(paths.root, 'assets/svg'),
    use: [
      {
        loader: "url-loader",
        options: {
          context: path.resolve(paths.root),
          limit: 10,
          name(file) {
            return isProd ? '[path][name].[ext]' : '[path][name].[hash].[ext]';
          }
        },
      }
    ]
  }
];

const plugins = [
  ...twigToHtml,
  new webpack.DefinePlugin({
    paths: JSON.stringify(paths),
    NODE_ENV: JSON.stringify(NODE_ENV)
  }),
  new MiniCssExtractPlugin({
    filename: !isProd ? 'css/[name].[hash].css' : 'css/[name].css',
    allChunks: false
  }),
  new CleanWebpackPlugin(paths.clean, {
    verbose: true
  }),
  new SpritesmithPlugin({
    src: {
      cwd: path.resolve(paths.root, 'assets/icons'),
      glob: '*.png'
    },
    target: {
      image: path.resolve(paths.root, 'assets/images/sprite.png'),
      css: path.resolve(paths.root, 'sass/common/_sprite.scss')
    },
    spritesmithOptions: {
      algorithm: 'diagonal',
      padding: 5,
      imgName: '../images/sprite.png',
    },
    apiOptions: {
      generateSpriteName: fullPathToSourceFile => {
        const {name} = path.parse(fullPathToSourceFile);
        return `sprite_${name}`;
      },
      cssImageRef: "../assets/images/sprite.png"
    }
  })
].concat(vuePlugins);

if (isProd) {
  imgRules.push({
    loader: 'image-webpack-loader',
    options: {
      bypassOnDebug: true,
      svgo: {
        plugins: [
          {
            cleanupIDs: false,
            removeUselessDefs: false,
            removeUselessStrokeAndFill: false
          }
        ]
      },
    }
  });
}


const webpackConfig = {
  entry: {
    index: path.resolve(paths.root, 'js/index.js'),
    contacts: path.resolve(paths.root, 'js/index.js'),
    blog: path.resolve(paths.root, 'js/index.js'),
    article: path.resolve(paths.root, 'js/index.js'),
    documents: path.resolve(paths.root, 'js/index.js'),
    company: path.resolve(paths.root, 'js/index.js'),
    team: path.resolve(paths.root, 'js/index.js'),
    houses270: path.resolve(paths.root, 'js/index.js'),
    'ready-and-build': path.resolve(paths.root, 'js/index.js'),
    villages: path.resolve(paths.root, 'js/index.js'),
    village: path.resolve(paths.root, 'js/index.js'),
    project: path.resolve(paths.root, 'js/index.js'),
    'as-we-build': path.resolve(paths.root, 'js/index.js'),
    'custom-design': path.resolve(paths.root, 'js/index.js'),
    'projects-catalog': path.resolve(paths.root, 'js/index.js'),
    'projects-catalog-no-result': path.resolve(paths.root, 'js/index.js'),
    'project-map': path.resolve(paths.root, 'js/index.js'),
    calculator: path.resolve(paths.root, 'js/index.js'),
  },
  output: {
    path: path.resolve(paths.public),
    chunkFilename: 'js/[name].bundle.js',
    publicPath: '/',
    filename: 'js/[name].js'
  },
  devServer: devServer,
  performance: {
    hints: false
  },
  resolve: {
    alias: {
      images: path.resolve(paths.root, 'assets/images/'),
      fonts: path.resolve(paths.root,'assets/fonts/'),
      components: path.resolve(paths.root, 'js/components'),
      modules: path.resolve(paths.root,'js/modules'),
      utils: path.resolve(paths.root,'js/utils'),
      ...vueAliases
    },
    extensions: ['.js', '.vue', '.json']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: {
          loader: "babel-loader",
          options: {
            cacheDirectory: true,
            presets: ['env'],
            plugins: ['syntax-dynamic-import']
          }
        }
      },
      {
        test: /\.twig$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: 'twig-loader'
          }
        ]
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: isProd ? MiniCssExtractPlugin.loader : isVue ? 'vue-style-loader': 'style-loader',
            options: isProd ? {
              publicPath: '../'
            } : {}
          },
          {
            loader: "css-loader",
            options: {
              minimize: isProd ? {discardComments: {removeAll: true}} : false,
              sourceMap: !isProd,
              alias: {
                images: path.resolve(paths.root, 'assets/images/')
              }
            }
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: [
                autoprefixer({
                  browsers: ['ie >= 9', 'last 4 version']
                })
              ]
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: !isProd,
              data: `@import "global";`,
              includePaths: [
                path.resolve(paths.root, "sass/")
              ]
            }
          },

        ]
      },
      {
        test: /\.ttf|woff|woff2$/,
        loader: 'file-loader',
        options: {
          name(file) {
            return isProd ? 'fonts/[name].[ext]' : 'fonts/[name].[hash].[ext]';
          }
        }
      }
    ].concat(imgRules).concat(vueRule)
  },
  plugins: plugins
};

module.exports = webpackConfig;
